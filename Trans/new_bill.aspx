﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="new_bill.aspx.cs" Inherits="Trans_new_bill" Title="New Bill" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد حذف هذه البيانات ?")) {
                confirm_value.value = "Yes";
             } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
      <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" align="left" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;" 
            dir="ltr">
        &nbsp;Create New Bill </span>
    </td></tr>
 </table>
 <table align="left" 
            
            
        style="width: 80%; height: 100px; float: right; margin-top: 50px; margin-right: 30px; " 
        dir="ltr">
            <tr>
                <td align="center">
            <asp:Label ID="lbl_MSG" runat="server" Font-Bold="True" ForeColor="Red" 
                        Font-Size="11pt" Font-Names="Arial"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                <td rowspan="12">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                EmptyDataText="No Billing Data" Caption="Billing Data" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" AutoGenerateSelectButton="True" 
                DataKeyNames="Bill_NO" onpageindexchanging="GridView1_PageIndexChanging" 
                onselectedindexchanging="GridView1_SelectedIndexChanging" Width="500px" 
                        Font-Names="Arial" Font-Size="13pt">
                
                <Columns>
                        <asp:TemplateField HeaderText="Bill No" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblbill_no" runat="server" Text='<%# Bind("Bill_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                             <asp:TemplateField HeaderText="Month">
                          <ItemTemplate>
                                <asp:Label ID="lblmonth" runat="server" Text='<%# Bind("MONTH") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                              <asp:TemplateField HeaderText="Year" >
                          <ItemTemplate>
                                <asp:Label ID="lblyear" runat="server" Text='<%# Bind("YEAR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Notes" Visible="false">
                          <ItemTemplate>
                                <asp:Label ID="lblnotes" runat="server" Text='<%# Bind("NOTE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Created User" Visible="false" >
                          <ItemTemplate>
                                <asp:Label ID="lblcreated_user" runat="server" Text='<%# Bind("CREATED_USER") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Created Date" Visible="false" >
                          <ItemTemplate>
                                <asp:Label ID="lblcreated_date" runat="server" Text='<%# Bind("CRAETED_DATE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Bill No" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_bill_no" runat="server" Width="92px" Font-Size="12pt" 
                        BackColor="#CCCCCC" Enabled="False"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txt_bill_no" ErrorMessage="Fill Data"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Month" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_month" runat="server" Width="110px" Font-Size="12pt"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txt_month" ErrorMessage="Fill Data"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" 
                        ControlToValidate="txt_month" ErrorMessage="Please enter month number" 
                        MaximumValue="12" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="Year" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_year" runat="server" Width="110px" Font-Size="12pt"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="txt_year" ErrorMessage="Fill Data"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" 
                        ControlToValidate="txt_year" ErrorMessage="Please enter the year" 
                        MaximumValue="2100" MinimumValue="2000" Type="Integer"></asp:RangeValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Note" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_notes" runat="server" Width="385px" Font-Size="12pt" 
                        Height="82px" TextMode="MultiLine"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" Font-Bold="True" 
                        Width="292px" />
                    <asp:Button ID="btnUpload" runat="server" Font-Bold="True" 
                        OnClick="btnUpload_Click" Text="Upload" Width="88px" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_count" runat="server" ForeColor="Blue"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Button ID="btn_new" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="New" Width="100px" 
                onclick="btn_new_Click" />
            <asp:Button ID="btn_save" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Save" Width="100px" 
                onclick="btn_save_Click" />
            <asp:Button ID="btn_update" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Update" Width="100px" 
                onclick="btn_update_Click" />
            <asp:Button ID="btn_delete" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Delete" Width="100px" 
                onclick="btn_delete_Click" onclientclick="Confirm()" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" 
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                        CellPadding="4" Font-Size="12pt" ForeColor="Black" GridLines="Vertical" 
                        OnPageIndexChanging="PageIndexChanging" PageSize="20">
                        <RowStyle BackColor="#F7F7DE" />
                        <FooterStyle BackColor="#CCCCCC" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

