﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.OleDb;
using System.IO;
using System.Configuration;
using Vodafone_bill_WS;


public partial class Trans_new_bill : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        lbl_MSG.Text = "";
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            txt_bill_no.Text = ser.Get_Next_Bill_ID();
            Fill_Grid();
            //*************************
            btn_update.Visible = false;
            btn_delete.Visible = false;
            btn_save.Visible = true;
            //**************************
        }
        check_security();
    }
    public void check_security()
    {
        try
        {
            string page_name = GetCurrentPageName();
            int Display = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DISPLAY");
            int Save = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "SAVE");
            int Update = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "UPDATE");
            int Delete = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DELETE");
            int All = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "ALL");

            if (All > 0)
            {
                //Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
            }
            else
            {
                if (Display > 0)
                {

                    //*******************************
                    if (Save > 0)
                    { btn_save.Visible = true; }
                    else
                    { btn_save.Visible = false; }
                    //******************************
                    if (Update > 0)
                    { btn_update.Visible = true; }
                    else
                    { btn_update.Visible = false; }
                    //*******************************
                    if (Delete > 0)
                    { btn_delete.Visible = true; }
                    else
                    { btn_delete.Visible = false; }
                    //*********************************

                }
                else
                {
                    Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
                }

            }
        }
        catch
        {

        }
    }
    public string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }
    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_Billing_GRID();
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Fill_Detail_Grid(string bill_no)
    {
        try
        {
            Clear_Details_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_Details_Billing_GRID(bill_no);
            GridView2.DataSource = Ds;
            GridView2.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Details_Grids()
    {
        Ds.Tables.Clear();
        GridView2.DataSource = null;
        GridView2.DataBind();
    }
    private void Clear_Grids()
    {
        Ds.Tables.Clear();
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["BILL_NO"] = ((Label)GridView1.SelectedRow.FindControl("lblbill_no")).Text;
            txt_bill_no.Text = ((Label)GridView1.SelectedRow.FindControl("lblbill_no")).Text;
            txt_year.Text = ((Label)GridView1.SelectedRow.FindControl("lblyear")).Text;
            txt_month.Text = ((Label)GridView1.SelectedRow.FindControl("lblmonth")).Text;
            txt_notes.Text = ((Label)GridView1.SelectedRow.FindControl("lblnotes")).Text;
            //***************************************************
            Fill_Detail_Grid(Session["BILL_NO"].ToString());
            //***************************************************
            btn_save.Visible = false;
            btn_update.Visible = true;
            btn_delete.Visible = true;
            //*************************************************
        }
        catch
        {
            Clear();
        }
        check_security();
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
            string Extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
            string FolderPath = ConfigurationManager.AppSettings["FolderPath"];

            string FilePath = Server.MapPath(FolderPath + FileName);
            FileUpload1.SaveAs(FilePath);
            Import_To_Grid(FilePath, Extension, "Yes");
        }
    }
    private void Import_To_Grid(string FilePath, string Extension, string isHDR)
    {
        string conStr = "";
        switch (Extension)
        {
            case ".xls": //Excel 97-03
                conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                break;
            case ".xlsx": //Excel 07
                conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                break;
        }
        try
        {

            if (Extension == ".xlsx" || Extension == ".xls")
            {
                conStr = String.Format(conStr, FilePath, isHDR);
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                //DataTable dt = new DataTable();
                cmdExcel.Connection = connExcel;

                //Get the name of First Sheet
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();

                //Read Data from First Sheet
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();

                //Bind Data to GridView
                GridView2.Caption = Path.GetFileName(FilePath);
                lbl_count.Text = " Total rows count " + dt.Rows.Count.ToString();
                Session["DETAILS"] = dt;
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
            else
            {
                Alert.Show(" you must select .xlsx or .xls Only  ");
                return;
            }
        }
        catch
        {
            lbl_MSG.Text = "Error .. Please Contact us .";
        }
    }
    protected void btn_new_Click(object sender, EventArgs e)
    {
        Clear();
    }
    private int Check_Master_Duplicate_Data(DataTable Enter_DT)
    {
        // string Value = "";
        int XX = 0;
        try
        {
            foreach (DataRow var in Enter_DT.Rows)
            {
                string LINE_NO = var[0].ToString();
                string RATE_PLAN = var[1].ToString();
                if (LINE_NO == string.Empty || LINE_NO == "")
                {
                    Alert.Show("برجاء التأكد من أن  طبيعة الخلية الخاصة بالأرقام لابد ان تكون ارقام وايضا لا يجب ان تكون فارغة");
                    return 0;
                }
                else
                {
                  
                    int duplicate = ser.Check_Duplicate_Line_No_In_bill(LINE_NO);
                    if (duplicate > 1)  // check duplicate lines 
                    {
                        XX++;
                    }
                }
            }


        }
        catch
        {

        }

        return XX;
    }

    private int Check_Master_Data(DataTable Enter_DT)
    {
       // string Value = "";
        int XX = 0;
        try
        {
            foreach (DataRow var in Enter_DT.Rows)
            {
                string LINE_NO = var[0].ToString();
                string RATE_PLAN = var[1].ToString();
                if (LINE_NO == string.Empty || LINE_NO == "")
                {
                    Alert.Show("برجاء التأكد من أن  طبيعة الخلية الخاصة بالأرقام لابد ان تكون ارقام وايضا لا يجب ان تكون فارغة");
                    return 0;
                }
                else
                {
                    string Status = ser.Check_Exist_Line(LINE_NO);
                  //  int duplicate = ser.Check_Duplicate_Line_No_In_bill(LINE_NO);
                    if (Status == "0")
                    {
                        XX++;
                        int YY = ser.Save_NOT_Saved_Lines(LINE_NO, RATE_PLAN);

                    }
                }
            }

           
        }
        catch
        {
 
        }

        return XX;
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            dt = (DataTable)Session["DETAILS"];
            if (txt_bill_no.Text == "" || txt_month.Text == "" || txt_year.Text == "" ||  dt == null )
            {
                lbl_MSG.Text = "Please Fill Empty Fields or upload detail Data";
                return;
            }
            else
            {
                if (dt.Rows.Count == 0)
                {
                    lbl_MSG.Text = " No Records Found ";
                    return;
                }
                int Detail = 0;
                int RESLT  = 0;
              
                int NOEXIST = Check_Master_Data(dt);
                if (NOEXIST > 0)
                {
                    Alert.Show("Sorry You have '"+ NOEXIST.ToString() +"' lines in this bill didn't have any Master Data" );
                  //  Response.Redirect("~/Master_Data/not_saved_data.aspx");
                    return;
                }
                int Duppl = Check_Master_Duplicate_Data(dt);
                if (Duppl > 0)
                {
                    Alert.Show("Sorry You have '" + Duppl.ToString() + "' lines in this bill saved with duplicate ");
                    return;
                }
                txt_bill_no.Text = ser.Get_Next_Bill_ID();
                RESLT = ser.Save_Bill_H_Data(txt_bill_no.Text, txt_month.Text, txt_year.Text, txt_notes.Text, Session["Logged_User"].ToString());
                foreach (DataRow var in dt.Rows)
                {
                    string LINE_NO = var[0].ToString();
                    string RAT_PLN = var[1].ToString();

                    string MON_CHRGS = var[2].ToString();
                    string NAT_CHRGS = var[3].ToString();

                    string INTR_CHRGS = var[4].ToString();
                    string ROM_CHRGS = var[5].ToString();
                    string OTHR_CHRGS = var[6].ToString();

                    Detail = ser.Save_Bill_D_Data(txt_bill_no.Text, LINE_NO, RAT_PLN, MON_CHRGS, NAT_CHRGS, INTR_CHRGS, ROM_CHRGS, OTHR_CHRGS);
                }
                if (RESLT == 1 && Detail == 1)
                {
                    Fill_Grid();
                    lbl_MSG.Text = "Data has been saved successfully";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = "Error in Saving Data ..!!";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in Saving Data ..!!";
        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_bill_no.Text == "" || txt_month.Text == "" || txt_year.Text == "")
            {
                lbl_MSG.Text = "Please Fill Empty Fields ";
                return;
            }
            else
            {

                int RESLT = ser.UPDATE_Bill_H_Data(Session["BILL_NO"].ToString(), txt_month.Text, txt_year.Text, txt_notes.Text);
                if (RESLT == 1)
                {
                    Fill_Grid();
                    lbl_MSG.Text = "Data has been updated successfully";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = "Error in updating Data ..!!";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in updating Data ..!!";
        }
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "No")
            {
                
            }
            else
            {
                if (txt_bill_no.Text == "")
                {
                    lbl_MSG.Text = "Please Fill Empty Fields";
                    return;
                }
                else
                {
                    int DETAIL = ser.DELETE_Bill_D_Data(Session["BILL_NO"].ToString());
                    int HEADER = ser.DELETE_Bill_H_Data(Session["BILL_NO"].ToString());

                    if (HEADER == 1 && DETAIL == 1)
                    {
                        Fill_Grid();
                        lbl_MSG.Text = "Data has been deleted successfully";
                        Clear();
                    }
                    else
                    {
                        lbl_MSG.Text = "Error in Deleting Data ..!!";
                    }
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in Deleting Data ..!!";
        }
    }
    private void Clear()
    {
        txt_month.Text = "";
        txt_year.Text = "";
        txt_notes.Text = "";
        lbl_count.Text = "";
        txt_bill_no.Text = ser.Get_Next_Bill_ID();
        Clear_Details_Grids();
        Session["DETAILS"] = null;
        Session["BILL_NO"] = null;
        dt.Clear();
        //*********************************
        btn_save.Visible = true;
        btn_update.Visible = false;
        btn_delete.Visible = false;
        //***********************************
        check_security();
    }
    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        string FolderPath = ConfigurationManager.AppSettings["FolderPath"];
        string FileName = GridView2.Caption;
        string Extension = Path.GetExtension(FileName);
        string FilePath = Server.MapPath(FolderPath + FileName);

        Import_To_Grid(FilePath, Extension, "Yes");
        GridView2.PageIndex = e.NewPageIndex;
        GridView2.DataBind();  
    }
}
