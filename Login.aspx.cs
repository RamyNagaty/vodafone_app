using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Vodafone_bill_WS;

public partial class Login : System.Web.UI.Page
{
   // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SQL_CONNECTION"].ConnectionString);
    SqlCommand com = new SqlCommand();
    SqlDataAdapter myadd = new SqlDataAdapter();
    DataSet myset = new DataSet();
    DataTable myDt = new DataTable();
    Vodafone_bill_WS.Service ser = new Service(); 


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Session.Clear();
    }
    protected void btn_enter_Click(object sender, EventArgs e)
    {
       
            bool Logged;
            
            Logged = ser.Check_Loged_User(txt_usr_name.Text, txt_password.Text);

            if (Logged  == true)
            {
                Session["Logged_User"] = txt_usr_name.Text.ToUpper();
                Session["USER_NAME"] = ser.Get_User_Name(txt_usr_name.Text.ToUpper()).ToString();
                Response.Redirect("~/Home_Page.aspx");
            }
            else
            {
               lbl_message.Text= " Invalid Password Kindly Check ..!! ";
                txt_password.Text = "";
                return;
            }
    }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        txt_usr_name.Text = "";
        txt_password.Text = "";
    }
}
