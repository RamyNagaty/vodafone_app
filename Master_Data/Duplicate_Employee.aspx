﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Duplicate_Employee.aspx.cs" Inherits="Master_Data_Duplicate_Employee" Title="Duplicate Employee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" align="left" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;" 
            dir="ltr">
        &nbsp;Duplicate Employees </span>
    </td></tr>
 </table>
 <table align="left" 
            
            
        style="width: 90%; height: 100px; float: right; margin-top: 50px; margin-right: 30px; " 
        dir="ltr">
            <tr>
                <td align="center">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td rowspan="13" align="center">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                EmptyDataText="Not Duplicates Found" Caption="Duplicate Employee" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" 
                DataKeyNames="EMP_NO" onpageindexchanging="GridView1_PageIndexChanging" 
                onselectedindexchanging="GridView1_SelectedIndexChanging" Width="500px" 
                        Font-Names="Arial" Font-Size="13pt">
                
                <Columns>
                        <asp:TemplateField HeaderText="Employee Code" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblline_no" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                             <asp:TemplateField HeaderText="Employee Name">
                          <ItemTemplate>
                                <asp:Label ID="lblrate_plan" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lbl_count" runat="server" Font-Bold="True" Font-Size="13pt"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

