﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Vodafone_bill_WS;

public partial class Master_Data_not_saved_data : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Bind_Not_Saved_Lines_GRID
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            Fill_Grid();
           lbl_count.Text = " Not saved Items count "+ ser.Get_not_saved_count()+ " Items ";
        }

        check_security();
    }

    public void check_security()
    {
        try
        {
            string page_name = GetCurrentPageName();
            int Display = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DISPLAY");
            int Save = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "SAVE");
            int Update = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "UPDATE");
            int Delete = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DELETE");
            int All = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "ALL");

            if (All > 0)
            {
                //Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
            }
            else
            {
                if (Display > 0)
                {

                    //*******************************
                    if (Save > 0)
                    { //btn_save.Visible = true; 
                    }
                    else
                    { //btn_save.Visible = false; 
                    }
                    //******************************
                    if (Update > 0)
                    { //btn_update.Visible = true; 
                    }
                    else
                    { //btn_update.Visible = false; 
                    }
                    //*******************************
                    if (Delete > 0)
                    { //btn_delete.Visible = true;
                    }
                    else
                    { //btn_delete.Visible = false; 
                    }
                    //*********************************

                }
                else
                {
                    Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
                }

            }
        }
        catch
        {

        }
    }
    public string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }

    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_Not_Saved_Lines_GRID();
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        Ds.Tables.Clear();
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
           Session["NS_LINE"] = ((Label)GridView1.SelectedRow.FindControl("lblline_no")).Text;
           Response.Redirect("~/Master_Data/new_line.aspx");

        }
        catch
        {
           
        }

        check_security();
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
}
