﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="new_line.aspx.cs" Inherits="Master_Data_new_line" Title="New Line Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد حذف هذه البيانات ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
      <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" align="left" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;" 
            dir="ltr">
        &nbsp; New Line </span>
    </td></tr>
 </table>
 <table align="left" 
            
            
        style="width: 80%; height: 100px; float: right; margin-top: 50px; margin-right: 30px; " 
        dir="ltr">
            <tr>
                <td align="center" bgcolor="#CCCCCC">
                    <asp:RadioButton ID="rd_all_lines" runat="server" Checked="True" 
                        Font-Bold="True" Font-Size="10pt" GroupName="R" Text="All Lines" />
                    <asp:RadioButton ID="rd_line_no" runat="server" Font-Bold="True" 
                        Font-Size="10pt" GroupName="R" Text="Line No" />
                    <asp:RadioButton ID="rd_emp_no" runat="server" Font-Bold="True" 
                        Font-Size="10pt" GroupName="R" Text="Emp. Code" />
                    <asp:RadioButton ID="rd_emp_name" runat="server" Font-Bold="True" 
                        Font-Size="10pt" GroupName="R" Text="Emp. Name" />
                </td>
                <td>
                    &nbsp;</td>
                <td rowspan="24">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                EmptyDataText="No Lines " Caption="Lines Data" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" AutoGenerateSelectButton="True" 
                DataKeyNames="Line_no" onpageindexchanging="GridView1_PageIndexChanging" 
                onselectedindexchanging="GridView1_SelectedIndexChanging" Width="500px" 
                        Font-Names="Arial" Font-Size="10pt" PageSize="25">
                
                <Columns>
                        <asp:TemplateField HeaderText="Line No" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblline_no" runat="server" Text='<%# Bind("LINE_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                             <asp:TemplateField HeaderText="Emp No">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_no" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                              <asp:TemplateField HeaderText="Name">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_name" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Department">
                          <ItemTemplate>
                                <asp:Label ID="lbldep_name" runat="server" Text='<%# Bind("SECTION_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Limit">
                          <ItemTemplate>
                                <asp:Label ID="lblapp_limit" runat="server" Text='<%# Bind("APP_LIMIT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Inter. Calls" Visible="False" >
                          <ItemTemplate>
                                <asp:Label ID="lblINTER_CALLS" runat="server" Text='<%# Bind("INTER_CALLS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="After Sales Tax" Visible="False">
                          <ItemTemplate>
                                <asp:Label ID="lblaftr_sal_tax" runat="server" Text='<%# Bind("AFTR_SAL_TAX") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Company Shar" Visible="False">
                          <ItemTemplate>
                                <asp:Label ID="lblcomp_shar" runat="server" Text='<%# Bind("COMP_SHAR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Employee Shar" Visible="False">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_shar" runat="server" Text='<%# Bind("EMP_SHAR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                      
                        <asp:TemplateField HeaderText="Notes" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblNotes" runat="server" Text='<%# Bind("NOTES") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="Created User" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbluser_name" runat="server" Text='<%# Bind("created_user") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Created Date" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblcreated_date" runat="server" Text='<%# Bind("created_date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="L_STATUS" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblL_STATUS" runat="server" Text='<%# Bind("L_STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="I_STATUS" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblI_STATUS" runat="server" Text='<%# Bind("I_STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Ref. Data" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblref_data" runat="server" Text='<%# Bind("REF_DATA") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                             <asp:TemplateField HeaderText="Recv. Date" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblrd_date" runat="server" Text='<%# Bind("RD_DATE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Delivery Date" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbldd_date" runat="server" Text='<%# Bind("DD_DATE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Stopped" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblstopped" runat="server" Text='<%# Bind("LINE_STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="Line Type" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblline_type" runat="server" Text='<%# Bind("LINE_TYPE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Rate Plan" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblrate_plan" runat="server" Text='<%# Bind("RATE_PLAN") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="Tax Flag" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbltaxflag" runat="server" Text='<%# Bind("TAX_FLAG") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="Tax Value" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbltaxvalue" runat="server" Text='<%# Bind("TAX_VAL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Tax Stamp" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblsaltax" runat="server" Text='<%# Bind("SAL_TAX_PER") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblid" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="table tax" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbltbltax" runat="server" Text='<%# Bind("tbl_tax_per") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="added tax" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbladded" runat="server" Text='<%# Bind("new_sales_tax_per") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center" bgcolor="#CCCCCC">
                    <asp:TextBox ID="txt_search" runat="server" BackColor="#CCFFFF" 
                        Font-Bold="True" Font-Size="12pt" Width="250px"></asp:TextBox>
                    <asp:Button ID="btn_search" runat="server" BackColor="#3399FF" 
                        CausesValidation="False" Font-Bold="True" onclick="btn_search_Click" 
                        Text="Search" Width="70px" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
            <asp:Label ID="lbl_MSG" runat="server" Font-Bold="True" ForeColor="Red" 
                        Font-Size="11pt" Font-Names="Arial"></asp:Label>
                </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Line No" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_line_no" runat="server" Font-Size="12pt"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                        ControlToValidate="txt_line_no" ErrorMessage="Only Numbers" 
                        ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txt_line_no" ErrorMessage="Fill Data"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="Emp. No" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:DropDownList ID="dd_emp_code" runat="server" Font-Bold="True" 
                        ForeColor="#3366FF" Width="270px" Font-Size="12pt">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Approved Limit" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_app_limit" runat="server" Width="150px" Font-Size="12pt"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="txt_app_limit" ErrorMessage="Only Numbers" 
                        ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txt_app_limit" ErrorMessage="Fill Data"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Inter. Calls" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_inter_calls" runat="server" Width="150px" Font-Size="12pt"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                        ControlToValidate="txt_inter_calls" ErrorMessage="Only Numbers" 
                        ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txt_inter_calls" ErrorMessage="Fill Data"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label15" runat="server" Font-Bold="True" Text="Ref. Name" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_ref_data" runat="server" Width="265px" Font-Size="12pt"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="Local Status" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:CheckBox ID="chk_local" runat="server" Font-Bold="True" Font-Size="12pt" 
                        Text="Unlimited local" />
                </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Inter. Status" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:CheckBox ID="chk_inter" runat="server" Font-Bold="True" Font-Size="12pt" 
                        Text="Unlimited International" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label16" runat="server" Font-Bold="True" Text="Receipt Date" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_rd_date" runat="server" Width="150px" Font-Size="12pt"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="dateValRegex" runat="server" 
                        ControlToValidate="txt_rd_date" ErrorMessage="Plz fill Valid Date" 
                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Delivery Date" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_dd_date" runat="server" Width="150px" Font-Size="12pt"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="dateValRegex0" runat="server" 
                        ControlToValidate="txt_dd_date" ErrorMessage="Plz fill Valid Date" 
                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label20" runat="server" Font-Bold="True" Text="Rate Plan" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_rat_plan" runat="server" Width="265px" Font-Size="12pt"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label19" runat="server" Font-Bold="True" Text="Line Type" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:DropDownList ID="dd_line_type" runat="server" Font-Bold="True" 
                        ForeColor="Red" Width="150px" Font-Size="12pt">
                        <asp:ListItem>Voice</asp:ListItem>
                        <asp:ListItem>Data</asp:ListItem>
                        <asp:ListItem>Land</asp:ListItem>
                        <asp:ListItem Value="DS">Data Solution</asp:ListItem>
                        <asp:ListItem Value="GPS">Data GPS</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label18" runat="server" Font-Bold="True" Text="Line Status" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:CheckBox ID="chk_line_status" runat="server" Font-Bold="True" Font-Size="12pt" 
                        Text="Stopped Line" ForeColor="#FF0066" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label23" runat="server" Font-Bold="True" Text="Tax Calc Flag." 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:CheckBox ID="chk_tax" runat="server" Font-Bold="True" Font-Size="12pt" 
                        Text="Calculated Tax" 
                        oncheckedchanged="chk_tax_CheckedChanged" Width="160px" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label25" runat="server" Font-Bold="True" Text="Stamp Tax " 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_stamptax" runat="server" Width="60px" Font-Size="12pt" 
                        BackColor="#FF3300" Font-Bold="True" ForeColor="White">0.00</asp:TextBox>
                    <asp:TextBox ID="txt_tax_unit" runat="server" Width="50px" Font-Size="12pt" 
                        BackColor="#FF3300" Enabled="False" Font-Bold="True" ForeColor="White">قرش</asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" 
                        ControlToValidate="txt_stamptax" ErrorMessage="Only Numbers In Stamp Tax" 
                        ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label24" runat="server" Font-Bold="True" Text="Sales Tax " 
                Width="110px" Font-Size="10pt" Font-Names="Arial" Visible="False"></asp:Label>
                    <asp:TextBox ID="txt_sales_tax" runat="server" Width="60px" Font-Size="12pt" 
                        BackColor="#FF3300" Font-Bold="True" ForeColor="White" ReadOnly="True" 
                        Visible="False">0.15</asp:TextBox>
                    <asp:TextBox ID="txt_percentage" runat="server" Width="50px" Font-Size="12pt" 
                        BackColor="#FF3300" Enabled="False" Font-Bold="True" ForeColor="White" 
                        ReadOnly="True" Visible="False">%</asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" 
                        ControlToValidate="txt_sales_tax" ErrorMessage="Only Numbers In Sales Tax" 
                        ValidationExpression="[0-9]*\.?[0-9]*" Visible="False"></asp:RegularExpressionValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td>
            <asp:Label ID="Label26" runat="server" Font-Bold="True" Text="Added Tax " 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_added_tax" runat="server" Width="60px" Font-Size="12pt" 
                        BackColor="#FF3300" Font-Bold="True" ForeColor="White" ReadOnly="True">0.14</asp:TextBox>
                    <asp:TextBox ID="txt_percentage0" runat="server" Width="50px" Font-Size="12pt" 
                        BackColor="#FF3300" Enabled="False" Font-Bold="True" ForeColor="White" 
                        ReadOnly="True">%</asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" 
                        ControlToValidate="txt_added_tax" ErrorMessage="Only Numbers In added Tax" 
                        ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td>
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Table Tax" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_tbl_tax" runat="server" Width="60px" Font-Size="12pt" 
                        BackColor="#FF3300" Font-Bold="True" ForeColor="White" ReadOnly="True">0.08</asp:TextBox>
                    <asp:TextBox ID="TextBox2" runat="server" Width="50px" Font-Size="12pt" 
                        BackColor="#FF3300" Enabled="False" Font-Bold="True" ForeColor="White">%</asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                        ControlToValidate="txt_tbl_tax" ErrorMessage="Only Numbers In Table Tax" 
                        ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr>
            <td>
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Notes" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td>
                    <asp:TextBox ID="txtnotes" runat="server" Height="116px" TextMode="MultiLine" 
                        Width="392px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label21" runat="server" Font-Bold="True" Text="Created User" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_user_name" runat="server" Width="265px" Font-Size="12pt" 
                        BackColor="#FFCC66" Enabled="False"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label22" runat="server" Font-Bold="True" Text="Creation Date" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_created_date" runat="server" Width="265px" 
                        Font-Size="12pt" BackColor="#FFCC66" Enabled="False"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Button ID="btn_new" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="New" Width="100px" 
                onclick="btn_new_Click" />
            <asp:Button ID="btn_save" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Save" Width="100px" 
                onclick="btn_save_Click" />
            <asp:Button ID="btn_update" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Update" Width="100px" 
                onclick="btn_update_Click" />
            <asp:Button ID="btn_delete" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Delete" Width="100px" 
                onclick="btn_delete_Click" onclientclick="Confirm()" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

