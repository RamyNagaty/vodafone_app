﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Vodafone_bill_WS;


public partial class Master_Data_Duplicate_Employee : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataSet Ds2 = new DataSet();
    DataTable dt = new DataTable();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
             Fill_Grid();
            lbl_count.Text = " Duplicate count " + ser.Get_Count_Duplicate_Employee() + " Items ";
        }

       // check_security();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }

    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Get_Duplicate_Employee();
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        Ds.Tables.Clear();
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
}
