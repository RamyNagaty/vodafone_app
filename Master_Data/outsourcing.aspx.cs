﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Vodafone_bill_WS;

public partial class Master_Data_outsourcing : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataSet Ds2 = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_MSG.Text = "";
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            Ds = ser.Bind_Dep_DDL();
            dd_dept.DataSource = Ds.Tables[0];
            dd_dept.DataTextField = "SECTION_NAME";
            dd_dept.DataValueField = "SECTION_NO";
            dd_dept.DataBind();
            ListItem lst = new ListItem("***** Select Department *****", "0");
            dd_dept.Items.Insert(0, lst);
            //**********************************
            Ds2 = ser.Bind_Job_DDL();
            dd_job.DataSource = Ds2.Tables[0];
            dd_job.DataTextField = "JOB_NAME";
            dd_job.DataValueField = "JOB_NO";
            dd_job.DataBind();
            ListItem lsta = new ListItem("***** Select Job *****", "0");
            dd_job.Items.Insert(0, lsta);
            //*********************************
            Fill_Grid();
            //********************************
            btn_update.Visible = false;
            btn_delete.Visible = false;
            btn_save.Visible = true;
            //**************************
        }

        check_security();
    }

    public void check_security()
    {
        try
        {
            string page_name = GetCurrentPageName();
            int Display = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DISPLAY");
            int Save = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "SAVE");
            int Update = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "UPDATE");
            int Delete = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DELETE");
            int All = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "ALL");

            if (All > 0)
            {
                //Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
            }
            else
            {
                if (Display > 0)
                {

                    //*******************************
                    if (Save > 0)
                    { btn_save.Visible = true; }
                    else
                    { btn_save.Visible = false; }
                    //******************************
                    if (Update > 0)
                    { btn_update.Visible = true; }
                    else
                    { btn_update.Visible = false; }
                    //*******************************
                    if (Delete > 0)
                    { btn_delete.Visible = true; }
                    else
                    { btn_delete.Visible = false; }
                    //*********************************

                }
                else
                {
                    Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
                }

            }
        }
        catch
        {

        }
    }
    public string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }

    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_OSE_GRID();
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        Ds.Tables.Clear();
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {

    }
    protected void btn_new_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            int status = ser.Check_OSE_In_PYEGIC(txt_emp_no.Text);
            if (txt_emp_no.Text == "" || dd_dept.SelectedIndex == 0 || dd_job.SelectedIndex == 0)
            {
                lbl_MSG.Text = "Please Fill Empty Fields ";
                return;
            }
            else if(status != 0 )
            {
                lbl_MSG.Text = "Sorry this employee already exist please verify your inputs. ";
                return;
            }
            else
            {

                int RESLT = ser.Save_OSE(txt_emp_no.Text, txt_emp_name.Text, dd_dept.SelectedValue.ToString(), dd_job.SelectedValue.ToString());
                if (RESLT == 1)
                {
                    Fill_Grid();
                 
                    lbl_MSG.Text = "Data has been saved successfully";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = "Error in Saving Data ..!!";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in Saving Data ..!!";
        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            int status = ser.Check_OSE_In_PYEGIC(txt_emp_no.Text);
            if (txt_emp_no.Text == "" || dd_dept.SelectedIndex == 0 || dd_job.SelectedIndex == 0)
            {
                lbl_MSG.Text = "Please Fill Empty Fields ";
                return;
            }
            else if (status != 0)
            {
                lbl_MSG.Text = "Sorry this employee already exist please verify your inputs. ";
                return;
            }
            else
            {

                int RESLT = ser.Update_OSE(txt_emp_no.Text, txt_emp_name.Text, dd_dept.SelectedValue.ToString(), dd_job.SelectedValue.ToString());
                if (RESLT == 1)
                {
                    Fill_Grid();
                 
                    lbl_MSG.Text = "Data has been updated successfully";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = " Error in updating Data ..!! ";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in Saving Data ..!!";
        }
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            //***********************************************************************************
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "No")
            {

            }
            else
            {
                int check_status = ser.Check_OSE_Have_Line(txt_emp_no.Text);
                if (txt_emp_no.Text == "" || dd_dept.SelectedIndex == 0 || dd_job.SelectedIndex == 0)
                {
                    lbl_MSG.Text = "Please Fill Empty Fields ";
                    return;
                }
                else if (check_status != 0)
                {
                    lbl_MSG.Text = "Sorry this employee have a line number you can't delete it . ";
                    return;
                }
                else
                {

                    int RESLT = ser.Delete_OSE(txt_emp_no.Text);
                    if (RESLT == 1)
                    {
                        Fill_Grid();
                      
                        lbl_MSG.Text = "Data has been Deleted successfully";
                        Clear();
                    }
                    else
                    {
                        lbl_MSG.Text = "Error in deleting Data ..!!";
                    }
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in deleting Data ..!!";
        }
    }
    private void Clear()
    {
        txt_emp_no.Text = "";
        txt_emp_name.Text = "";
        txt_search.Text = "";
        dd_dept.ClearSelection();
        dd_job.ClearSelection();
        dd_dept.Items[0].Selected = true;
        dd_job.Items[0].Selected = true;

        //*********************************
        btn_save.Visible = true;
        btn_update.Visible = false;
        btn_delete.Visible = false;
        //***********************************
        check_security();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["OSE_NO"] = ((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text;
            txt_emp_no.Text = ((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text;
            txt_emp_name.Text = ((Label)GridView1.SelectedRow.FindControl("lblemp_name")).Text;

           string dep = ((Label)GridView1.SelectedRow.FindControl("lbldep")).Text;
           if (dep != "")
           {
               dd_dept.ClearSelection();
               dd_dept.Items.FindByText(dep).Selected = true;
           }
           string job = ((Label)GridView1.SelectedRow.FindControl("lbljob")).Text;
           if (job != "")
           {
               dd_job.ClearSelection();
               dd_job.Items.FindByText(job).Selected = true;
           }
            //******************
           btn_save.Visible = false;
           btn_update.Visible = true;
           btn_delete.Visible = true;
        }
        catch
        {
            Clear();
        }

        check_security();
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
}
