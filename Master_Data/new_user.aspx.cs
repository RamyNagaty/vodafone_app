﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Vodafone_bill_WS;

public partial class Master_Data_new_user : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_MSG.Text = "";
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            txt_ID.Text = ser.Get_Next_User_ID();

            //**********************************
            Fill_Grid();
            //*************************
            rd_open.Checked = true;
            btn_update.Visible = false;
            btn_delete.Visible = false;
            btn_save.Visible = true;
            //**************************
        }

        check_security();
    }

    public void check_security()
    {
        try
        {
            string page_name = GetCurrentPageName();
            int Display = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DISPLAY");
            int Save = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "SAVE");
            int Update = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "UPDATE");
            int Delete = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DELETE");
            int All = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "ALL");

            if (All > 0)
            {
                //Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
            }
            else
            {
                if (Display > 0)
                {

                    //*******************************
                    if (Save > 0)
                    { btn_save.Visible = true; }
                    else
                    { btn_save.Visible = false; }
                    //******************************
                    if (Update > 0)
                    { btn_update.Visible = true; }
                    else
                    { btn_update.Visible = false; }
                    //*******************************
                    if (Delete > 0)
                    { btn_delete.Visible = true; }
                    else
                    { btn_delete.Visible = false; }
                    //*********************************

                }
                else
                {
                    Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
                }

            }
        }
        catch
        {

        }
    }
    public string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }

    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_Users_GRID();
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        Ds.Tables.Clear();
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["USER_ID"] = ((Label)GridView1.SelectedRow.FindControl("lbluser_id")).Text;
            txt_ID.Text = ((Label)GridView1.SelectedRow.FindControl("lbluser_id")).Text;
            txt_user_name.Text = ((Label)GridView1.SelectedRow.FindControl("lbluser_name")).Text;
            txt_password.Text = ((Label)GridView1.SelectedRow.FindControl("lblpassword")).Text;
            txt_full_name.Text = ((Label)GridView1.SelectedRow.FindControl("lblfull_name")).Text;
            string status = ((Label)GridView1.SelectedRow.FindControl("lblstatus")).Text;
            if (status.Trim() == "0") { rd_closed.Checked = true; rd_open.Checked = false; }
            else if (status.Trim() == "1") { rd_open.Checked = true; rd_closed.Checked = false; }
            //***************************************************
            btn_save.Visible = false;
            btn_update.Visible = true;
            btn_delete.Visible = true;
            //*************************************************
        }
        catch
        {
            Clear();
        }

        check_security();
    }
    private void Clear()
    {

        txt_user_name.Text = "";
        txt_password.Text = "";
        txt_full_name.Text = "";
        txt_ID.Text = ser.Get_Next_User_ID();
        Session["USER_ID"] = "";
        //*********************************
        btn_save.Visible = true;
        btn_update.Visible = false;
        btn_delete.Visible = false;
        //***********************************
        check_security();
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            //***********************************************************************************
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "No")
            {

            }
            else
            {

                if (txt_ID.Text == "" || txt_user_name.Text == "")
                {
                    lbl_MSG.Text = "Please Fill Empty Fields ";
                    return;
                }
                else
                {

                    int RESLT = ser.Delete_Users_Data(Session["USER_ID"].ToString());
                    if (RESLT == 1)
                    {
                        Fill_Grid();
                        lbl_MSG.Text = "Data has been Deleted successfully";
                        Clear();
                    }
                    else
                    {
                        lbl_MSG.Text = "Error in deleting Data ..!!";
                    }
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in deleting Data ..!!";
        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_ID.Text == "" || txt_user_name.Text == "")
            {
                lbl_MSG.Text = "Please Fill Empty Fields ";
                return;
            }
            else
            {

                int RESLT = ser.Update_Users_Data(Session["USER_ID"].ToString(), txt_user_name.Text.ToUpper(), txt_full_name.Text, txt_password.Text, Get_Status());
                if (RESLT == 1)
                {
                    Fill_Grid();
                    lbl_MSG.Text = "Data has been updated successfully";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = "Error in updating Data ..!!";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in Saving Data ..!!";
        }
    }
    private string Get_Status()
    {
        string status = "";
        if (rd_open.Checked == true)
        { status = "1"; }
        else if (rd_closed.Checked == true)
        { status = "0"; }

        return status;
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_ID.Text == "" || txt_user_name.Text == "" )
            {
                lbl_MSG.Text = "Please Fill Empty Fields ";
                return;
            }
            else
            {
                 txt_ID.Text = ser.Get_Next_User_ID();
                 int RESLT = ser.Save_Users_Data(txt_ID.Text, txt_user_name.Text.ToUpper(), txt_full_name.Text, txt_password.Text, Get_Status());
                if (RESLT == 1)
                {
                    Fill_Grid();
                    lbl_MSG.Text = "Data has been saved successfully";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = "Error in Saving Data ..!!";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in Saving Data ..!!";
        }
    }
    protected void btn_new_Click(object sender, EventArgs e)
    {
        Clear();
    }
}
