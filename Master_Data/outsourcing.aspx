﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="outsourcing.aspx.cs" Inherits="Master_Data_outsourcing" Title=" New Out sourcing employee " %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد حذف هذه البيانات ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
      <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" align="left" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;" 
            dir="ltr">
        &nbsp; New outsourced employee </span>
    </td></tr>
 </table>
 <table align="left" 
            
            
        style="width: 80%; height: 100px; float: right; margin-top: 50px; margin-right: 30px; " 
        dir="ltr">
            <tr>
                <td align="center">
                    <asp:RadioButton ID="rd_all_lines" runat="server" Checked="True" 
                        Font-Bold="True" Font-Size="10pt" GroupName="R" Text="All Emlpyees" 
                        Visible="False" />
                    <asp:RadioButton ID="rd_line_no" runat="server" Font-Bold="True" 
                        Font-Size="10pt" GroupName="R" Text="Employee Name" Visible="False" />
                    <asp:RadioButton ID="rd_emp_no" runat="server" Font-Bold="True" 
                        Font-Size="10pt" GroupName="R" Text="Employee No" Visible="False" />
                </td>
                <td>
                    &nbsp;</td>
                <td rowspan="14">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                EmptyDataText="No outsourced employee" Caption="Outsourced Employee" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" AutoGenerateSelectButton="True" 
                DataKeyNames="EMP_NO" onpageindexchanging="GridView1_PageIndexChanging" 
                onselectedindexchanging="GridView1_SelectedIndexChanging" Width="500px" 
                        Font-Names="Arial" Font-Size="10pt">
                
                <Columns>
                        <asp:TemplateField HeaderText="Emp. No" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblemp_no" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                             <asp:TemplateField HeaderText="Name">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_name" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                              <asp:TemplateField HeaderText="Department">
                          <ItemTemplate>
                                <asp:Label ID="lbldep" runat="server" Text='<%# Bind("SECTION_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Job Name">
                          <ItemTemplate>
                                <asp:Label ID="lbljob" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:TextBox ID="txt_search" runat="server" BackColor="#CCFFFF" 
                        Font-Bold="True" Font-Size="12pt" Width="250px" Visible="False"></asp:TextBox>
                    <asp:Button ID="btn_search" runat="server" BackColor="#3399FF" 
                        CausesValidation="False" Font-Bold="True" onclick="btn_search_Click" 
                        Text="Search" Width="70px" Visible="False" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
            <asp:Label ID="lbl_MSG" runat="server" Font-Bold="True" ForeColor="Red" 
                        Font-Size="11pt" Font-Names="Arial"></asp:Label>
                </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Emp No" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_emp_no" runat="server" Font-Size="12pt"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txt_emp_no" ErrorMessage="Fill Data"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label16" runat="server" Font-Bold="True" Text="Employee Name" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:TextBox ID="txt_emp_name" runat="server" Width="267px" Font-Size="12pt"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="Department" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:DropDownList ID="dd_dept" runat="server" Font-Bold="True" 
                        ForeColor="#3366FF" Width="270px" Font-Size="12pt">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Job Name" 
                Width="110px" Font-Size="10pt" Font-Names="Arial"></asp:Label>
                    <asp:DropDownList ID="dd_job" runat="server" Font-Bold="True" 
                        ForeColor="#3366FF" Width="270px" Font-Size="12pt">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Button ID="btn_new" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="New" Width="100px" 
                onclick="btn_new_Click" />
            <asp:Button ID="btn_save" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Save" Width="100px" 
                onclick="btn_save_Click" />
            <asp:Button ID="btn_update" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Update" Width="100px" 
                onclick="btn_update_Click" />
            <asp:Button ID="btn_delete" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="Delete" Width="100px" 
                onclick="btn_delete_Click" onclientclick="Confirm()" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

