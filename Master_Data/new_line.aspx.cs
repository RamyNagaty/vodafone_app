﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Vodafone_bill_WS;

public partial class Master_Data_new_line : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_MSG.Text = "";
        
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            Ds = ser.Bind_EMP_DDL();
            dd_emp_code.DataSource = Ds.Tables[0];

            dd_emp_code.DataTextField = "EMP_NAME";
            dd_emp_code.DataValueField = "EMP_NO";
            dd_emp_code.DataBind();
            ListItem lst = new ListItem("***** Select Employee *****", "0");
            dd_emp_code.Items.Insert(0, lst);
            //**********************************
            Fill_Grid();
            //*************************
            if (Session["NS_LINE"] != null)
            { txt_line_no.Text = Session["NS_LINE"].ToString(); }
            //***********************
           // txt_stamptax.Enabled = false;
            btn_update.Visible = false;
            btn_delete.Visible = false;
            btn_save.Visible = true;
            //**************************
        }

        check_security();
    }
    public void check_security()
    {
        try
        {
            string page_name = GetCurrentPageName();
            int Display = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DISPLAY");
            int Save = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "SAVE");
            int Update = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "UPDATE");
            int Delete = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DELETE");
            int All = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "ALL");

            if (All > 0) 
            {
                //Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
            }
            else
            {
                if (Display > 0)
                {

                    //*******************************
                    if (Save > 0)
                    { btn_save.Visible = true; }
                    else
                    { btn_save.Visible = false; }
                     //******************************
                    if (Update > 0)
                    { btn_update.Visible = true; }
                    else
                    { btn_update.Visible = false; }
                    //*******************************
                    if (Delete > 0)
                    { btn_delete.Visible = true; }
                    else
                    { btn_delete.Visible = false; }
                    //*********************************

                }
                else
                {
                    Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
                }

            }
        }
        catch
        {
 
        }
    }
    public string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }
    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_LINES_GRID();
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Fill_Grid_By_Line( string Line_no )
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_LINES_GRID_By_Line(Line_no);
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Fill_Grid_By_Emp(string Emp_no)
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_LINES_GRID_By_Emp(Emp_no);
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Fill_Grid_By_Emp_Name(string Emp_name)
    {
        try
        {
            Clear_Grids();
            Ds.Tables.Clear();
            Ds = ser.Bind_LINES_GRID_By_NAME(Emp_name);
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        Ds.Tables.Clear();
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["LINE_NO"] = ((Label)GridView1.SelectedRow.FindControl("lblline_no")).Text;
            txt_line_no.Text = ((Label)GridView1.SelectedRow.FindControl("lblline_no")).Text;
            string emp_name = ((Label)GridView1.SelectedRow.FindControl("lblemp_name")).Text;
            string emp_no = ((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text;
            if (emp_name != "")
            {
                dd_emp_code.ClearSelection();
                dd_emp_code.Items.FindByValue(emp_no).Selected = true;
            }

            txt_app_limit.Text = ((Label)GridView1.SelectedRow.FindControl("lblapp_limit")).Text;
            txt_inter_calls.Text = ((Label)GridView1.SelectedRow.FindControl("lblinter_calls")).Text;
           // txt_TAST.Text = ((Label)GridView1.SelectedRow.FindControl("lblaftr_sal_tax")).Text;
           // txt_comp_shar.Text = ((Label)GridView1.SelectedRow.FindControl("lblcomp_shar")).Text;
           // txt_emp_shar.Text = ((Label)GridView1.SelectedRow.FindControl("lblemp_shar")).Text;
            txtnotes.Text = ((Label)GridView1.SelectedRow.FindControl("lblnotes")).Text;
            string Local_Status = ((Label)GridView1.SelectedRow.FindControl("lblL_STATUS")).Text;
            string Inter_Status = ((Label)GridView1.SelectedRow.FindControl("lblI_STATUS")).Text;
            if (Local_Status.Trim() == "1")
            { chk_local.Checked = true; }
            else
            { chk_local.Checked = false; }
            //***********************************
            if (Inter_Status.Trim() == "1")
            { chk_inter.Checked = true; }
            else
            { chk_inter.Checked = false; }
            //***************************************************
            txt_ref_data.Text = ((Label)GridView1.SelectedRow.FindControl("lblref_data")).Text;
            txt_rd_date.Text = ((Label)GridView1.SelectedRow.FindControl("lblrd_date")).Text;
            txt_dd_date.Text = ((Label)GridView1.SelectedRow.FindControl("lbldd_date")).Text;
            string line_status = ((Label)GridView1.SelectedRow.FindControl("lblstopped")).Text;
            if (line_status.Trim() == "1")
            { chk_line_status.Checked = true; }
            else
            { chk_line_status.Checked = false; }
            txt_rat_plan.Text = ((Label)GridView1.SelectedRow.FindControl("lblrate_plan")).Text;
            string line_type = ((Label)GridView1.SelectedRow.FindControl("lblline_type")).Text;
            if (line_type != "")
            {
                dd_line_type.ClearSelection();
                dd_line_type.Items.FindByValue(line_type).Selected = true;
            }

            string tax_flag = ((Label)GridView1.SelectedRow.FindControl("lbltaxflag")).Text;
            Set_Tax_Status(tax_flag);
            txt_stamptax.Text = ((Label)GridView1.SelectedRow.FindControl("lbltaxvalue")).Text;
            txt_sales_tax.Text = ((Label)GridView1.SelectedRow.FindControl("lblsaltax")).Text;

            txt_added_tax.Text = ((Label)GridView1.SelectedRow.FindControl("lbladded")).Text;

            txt_user_name.Text = ((Label)GridView1.SelectedRow.FindControl("lbluser_name")).Text;
            txt_created_date.Text = ((Label)GridView1.SelectedRow.FindControl("lblcreated_date")).Text;

            Session["ID"] = ((Label)GridView1.SelectedRow.FindControl("lblid")).Text;
            btn_save.Visible = false;
            btn_update.Visible = true;
            btn_delete.Visible = true;
            //*************************************************
        }
        catch
        {
            Clear();
        }

        check_security();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    private void Clear()
    {
       
        txt_app_limit.Text = "";
       // txt_comp_shar.Text = "";
        //txt_emp_shar.Text = "";
        txt_inter_calls.Text = "";
        txt_line_no.Text = "";
        txt_dd_date.Text = "";
        txt_rd_date.Text = "";
        txtnotes.Text = "";
        txt_ref_data.Text = "";
        txt_rat_plan.Text = "";
        txt_user_name.Text = "";
        txt_created_date.Text = "";
        txt_stamptax.Text = "0.00";
        txt_sales_tax.Text = "0.15" ;
        txt_tbl_tax.Text = "0.08";
        txt_added_tax.Text = "0.14";
        txt_stamptax.Enabled = false;

        dd_emp_code.ClearSelection();
        dd_emp_code.Items[0].Selected = true;
        dd_line_type.SelectedIndex = 0;

        chk_inter.Checked = false;
        chk_local.Checked = false;
        chk_line_status.Checked = false;
        chk_tax.Checked = false;
        //*********************************
        btn_save.Visible = true;
        btn_update.Visible = false;
        btn_delete.Visible = false;
        //***********************************

        check_security();
    }
    private string Get_LCL_STATUS()
    {
        string XX = "0";
        try
        {
            if (chk_local.Checked == true)
            { XX = "1"; }
            else
            { XX = "0"; }
        }
        catch
        {
            XX = "0";
        }
        return XX;
    }
    private string Get_INTR_STATUS()
    {
        string XX = "0";
        try
        {
            if (chk_inter.Checked == true)
            { XX = "1"; }
            else
            { XX = "0"; }
        }
        catch
        {
            XX = "0";
        }
        return XX;
    }
    private string Get_Line_Status()
    {
        string XX = "0";
        try
        {
            if (chk_line_status.Checked == true)
            { XX = "1"; }
            else
            { XX = "0"; }
        }
        catch
        {
            XX = "0";
        }
        return XX;
    }
    private string Get_Tax_Status()
    {
        string XX = "0";
        try
        {
            if (chk_tax.Checked == true)
            { XX = "1"; }
            else
            { XX = "0"; }
        }
        catch
        {
            XX = "0";
        }
        return XX;
    }
    private void Set_Tax_Status( string val )
    {
        try
        {
            if (val.Trim() == "1")
            { 
                chk_tax.Checked = true;
                txt_stamptax.Enabled = true;
            }
            else
            {
                chk_tax.Checked = false;
                txt_stamptax.Enabled = false;
            }
        }
        catch
        {
            chk_tax.Checked = false;
        }

    }
    protected void btn_new_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_line_no.Text == "" || dd_emp_code.SelectedIndex == 0 )
            {
                lbl_MSG.Text = "Please Fill Empty Fields ";
                return;
            }
            else
            {
                int Check = ser.Check_Saved_Line_No(txt_line_no.Text, dd_emp_code.SelectedValue.ToString());
                int Check_Status = ser.Check_Line_No(txt_line_no.Text);
                if (Check != 0 || Check_Status != 0)
                {
                    Alert.Show("this line number is already exist with this employee or other please check  ");
                    return;
                }
                else
                {
                    int RESLT = ser.Save_Lines_Data(
                            txt_line_no.Text, dd_emp_code.SelectedValue.ToString(), 
                            txt_app_limit.Text, txt_inter_calls.Text, "0", "0", "0", 
                            Session["Logged_User"].ToString(), txtnotes.Text, Get_LCL_STATUS(), 
                            Get_INTR_STATUS(), txt_ref_data.Text, txt_rd_date.Text, txt_dd_date.Text, 
                            Get_Line_Status().ToString(), txt_rat_plan.Text, dd_line_type.SelectedValue.ToString(), 
                            Get_Tax_Status().ToString(), txt_stamptax.Text , txt_sales_tax.Text , txt_tbl_tax.Text , txt_added_tax.Text );
                    if (RESLT == 1)
                    {
                        Fill_Grid();
                        int XX = ser.Delete_NOT_Saved_Lines(txt_line_no.Text);
                        lbl_MSG.Text = "Data has been saved successfully";
                        Clear();
                    }
                    else
                    {
                        lbl_MSG.Text = "Error in Saving Data ..!!";
                    }
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in Saving Data ..!!";
        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_line_no.Text == "" || dd_emp_code.SelectedIndex == 0)
            {
                lbl_MSG.Text = "Please Fill Empty Fields ";
                return;
            }
            else
            {
                 //int Check = ser.Check_Saved_Line_No(txt_line_no.Text, dd_emp_code.SelectedValue.ToString());
                 //if (Check != 0)
                 //{
                 //    Alert.Show(" هذا الرقم ساري ومسجل من قبل علي نفس الموظف  ");
                 //    return;
                 //}
                 //else
                 //{
                    int RESLT = ser.Update_Lines_Data(Session["ID"].ToString() , txt_line_no.Text, dd_emp_code.SelectedValue.ToString(), txt_app_limit.Text, txt_inter_calls.Text, "0", "0", "0", txtnotes.Text, Get_LCL_STATUS(), Get_INTR_STATUS(), txt_ref_data.Text, txt_rd_date.Text, txt_dd_date.Text, Get_Line_Status().ToString(), txt_rat_plan.Text, dd_line_type.SelectedValue.ToString(), Get_Tax_Status().ToString(), txt_stamptax.Text, txt_sales_tax.Text , txt_tbl_tax.Text , txt_added_tax.Text );
                     if (RESLT == 1)
                     {
                         Fill_Grid();
                         int XX = ser.Delete_NOT_Saved_Lines(txt_line_no.Text);
                         lbl_MSG.Text = "Data has been updated successfully";
                         Clear();
                     }
                     else
                     {
                         lbl_MSG.Text = " Error in updating Data ..!! ";
                     }
                 //}
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in Saving Data ..!!";
        }
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            //***********************************************************************************
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "No")
            {
               
            }
            else
            {

                if (txt_line_no.Text == "" || dd_emp_code.SelectedIndex == 0)
                {
                    lbl_MSG.Text = "Please Fill Empty Fields ";
                    return;
                }
                else
                {

                    int RESLT = ser.Delet_Lines_Data(Session["LINE_NO"].ToString(), dd_emp_code.SelectedValue.ToString(), Session["ID"].ToString());
                    if (RESLT == 1)
                    {
                        Fill_Grid();
                        int XX = ser.Delete_NOT_Saved_Lines(txt_line_no.Text);
                        lbl_MSG.Text = "Data has been Deleted successfully";
                        Clear();
                    }
                    else
                    {
                        lbl_MSG.Text = " Error in deleting Data ..!!";
                    }
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "Error in deleting Data ..!!";
        }
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        if (rd_all_lines.Checked == true)
        {
            Fill_Grid();
        }
        else if (rd_line_no.Checked == true)
        {
            Fill_Grid_By_Line(txt_search.Text);
        }
        else if (rd_emp_no.Checked == true)
        {
            Fill_Grid_By_Emp(txt_search.Text);
        }
        else if (rd_emp_name.Checked == true)
        {
            Fill_Grid_By_Emp_Name(txt_search.Text);
        }
    }
    protected void chk_tax_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_tax.Checked == true)
        {
            txt_stamptax.Enabled = true;
        }
        else
        {
            txt_stamptax.Text = "0.00";
            txt_stamptax.Enabled = false;
        }
    }
}
