<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Login Page</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 100px;
            float: right;
        }
    </style>
</head>
<body style="background-image: url('Images/cs-vodafone.png'); background-repeat: no-repeat; background-attachment: fixed; background-position: center top">
    <form id="form1" runat="server">
    <div>
        <div style="text-align: center">
            <br />
            <br />
            <br />
            <table align="center" class="style1" dir="rtl">
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                            &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <table style="width: 100%" style="height: 211px">
                <tr>
                    <td align="center" colspan="3" style="height: 21px">
                        <asp:Label ID="Label5" runat="server" Font-Bold="True" 
                Font-Italic="False" Font-Names="Arial"
                            Font-Size="20pt" ForeColor="Red" Width="506px">Vodafone Business Billing Manager</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3" style="height: 21px">
                            <asp:Image ID="Image2" runat="server" Height="91px" 
                            ImageUrl="~/Images/vv.png" Width="135px" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3" style="height: 21px">
                            <table align="center" dir="ltr">
                                <tr>
                                    <td colspan="2" style="height: 21px" align="center">
                                        <asp:Label ID="Label3" runat="server" BackColor="Gray" ForeColor="White" Text="Security"
                                            Width="254px" Font-Size="15pt" Font-Names="Arial"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 21px">
                                        <asp:Label ID="lbl_message" runat="server" Font-Size="12pt" ForeColor="Red" 
                                            Width="224px" Font-Names="Arial"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" align="center" dir="rtl">
                                        <asp:Label ID="Label1" runat="server" ForeColor="#404040" Text="User Name" 
                                            Width="110px" Font-Size="14pt" Font-Names="Arial" Font-Bold="True"></asp:Label></td>
                                    <td dir="ltr">
                                        <asp:TextBox ID="txt_usr_name" runat="server" Width="150px" Font-Size="15pt" 
                                            Font-Bold="True"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" dir="rtl">
                                        <asp:Label ID="Label2" runat="server" ForeColor="#404040" Text="Password" 
                                            Width="110px" Font-Size="14pt" Font-Names="Arial" Font-Bold="True"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txt_password" runat="server" TextMode="Password" Width="152px" 
                                            Font-Size="15pt" Font-Bold="True"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2" width="100%">
                                        <asp:Button ID="btn_cancel" runat="server" BackColor="#99CCFF" Font-Bold="True" Text="Enter"
                                            Width="150px" OnClick="btn_enter_Click" TabIndex="1" Font-Size="15pt" 
                                            BorderStyle="Solid" Font-Names="Arial" />
                                        </td>
                                </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3" dir="rtl">
                        &nbsp; &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 96px; height: 21px">
                    </td>
                    <td style="width: 87px; height: 21px">
                        &nbsp;</td>
                    <td style="width: 100px; height: 21px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 96px; height: 21px">
                    </td>
                    <td style="width: 87px; height: 21px">
                        &nbsp;</td>
                    <td style="width: 100px; height: 21px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 96px">
                    </td>
                    <td style="width: 87px">
                        &nbsp;</td>
                    <td style="width: 100px">
                    </td>
                </tr>
            </table>
        </div>
    
    </div>
    </form>
</body>
</html>
