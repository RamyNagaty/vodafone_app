﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="lines_report.aspx.cs" Inherits="Reports_lines_report" Title=" Lines Report " %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript">
            function doPrint() {
                var prtContent = document.getElementById('<%= ReportViewer1.ClientID %>');
        prtContent.border = 0; //set no border here
        var WinPrint = window.open('', '', 'left=150,top=100,width=1000,height=1000,toolbar=0,scrollbars=1,status=0,resizable=1');
        WinPrint.document.write(prtContent.outerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
        WinPrint.close();
    }
   </script> 
    <table style="width: 100%;" align="left"  >
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:12pt; font-weight:bold; color:#000080; font-family:Courier New;" 
            >
        &nbsp; Lines Data By Employees </span>
    </td></tr>
 </table>
        <br />
        <br />
          <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server"> </asp:ScriptManager>
        <br />
  
 <table  align="left" width="100%"  dir="ltr" 
        style="margin-top: 30px; margin-right: 30px; margin-left: 30px">
        <tr>
            <td align="left" dir="ltr">
              
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="Employee" Width="140px"></asp:Label>
                &nbsp;<asp:DropDownList ID="dd_emp_code" runat="server" Font-Bold="True" 
                        ForeColor="#3366FF" Width="270px" Font-Size="13pt">
                    </asp:DropDownList>
                <br />
         
                <br />
     
            </td>
        </tr>
        <tr>
            <td align="left" dir="ltr">
              
                <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="Department" Width="140px"></asp:Label>
                    <asp:DropDownList ID="dd_dept" runat="server" Font-Bold="True" 
                        ForeColor="#3366FF" Width="270px" Font-Size="13pt" 
                    onselectedindexchanged="dd_dept_SelectedIndexChanged">
                    </asp:DropDownList>
     
            </td>
        </tr>
        <tr>
            <td align="left" dir="ltr">
              
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="Line Type" Width="140px"></asp:Label>
                    <asp:DropDownList ID="dd_line_type" runat="server" Font-Bold="True" 
                        ForeColor="Red" Width="150px" Font-Size="12pt" 
                    onselectedindexchanged="dd_line_type_SelectedIndexChanged">
                        <asp:ListItem Value="0">All Types</asp:ListItem>
                        <asp:ListItem>Voice</asp:ListItem>
                        <asp:ListItem>Data</asp:ListItem>
                        <asp:ListItem>Land</asp:ListItem>
                        <asp:ListItem Value="DS">Data Solution</asp:ListItem>
                        <asp:ListItem Value="GPS">Data GPS</asp:ListItem>
                        
                    </asp:DropDownList>
     
            </td>
        </tr>
        <tr>
            <td align="left" dir="ltr">
              
                <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="Status" Width="140px"></asp:Label>
                <asp:CheckBox ID="dd_status" runat="server" 
                    oncheckedchanged="dd_status_CheckedChanged" Text="Active" Font-Bold="True" 
                    Font-Size="12pt" />
     
            </td>
        </tr>
        <tr>
            <td align="left" dir="ltr">
              
                <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="Emp. Status" Width="140px"></asp:Label>
                <asp:CheckBox ID="dd_finished" runat="server" 
                    oncheckedchanged="dd_status_CheckedChanged" Text="Finished" 
                    Font-Bold="True" Font-Size="12pt" />
     
            </td>
        </tr>
        <tr>
            <td align="left">
         
        <asp:Button ID="btn_show" runat="server" Font-Bold="True" Font-Size="12pt" 
            onclick="btn_show_Click" Text="Display Report" Width="150px" />
     
            </td>
        </tr>
        <tr>
            <td align="left">
         
    <asp:Button ID="PrintButton" runat="server" Text="Print" OnClientClick="doPrint();" 
            ToolTip="Print Report" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" 
                      Width="150px" />
     
            </td>
        </tr>
        <tr>
            <td align="left">
         
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="500px" 
                    Width="99%">
                </rsweb:ReportViewer>
     
            </td>
        </tr>
        <tr align="right" dir="rtl">
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
 
        <br />
        <br />


</asp:Content>

