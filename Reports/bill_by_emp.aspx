﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="bill_by_emp.aspx.cs" Inherits="Reports_bill_by_emp" Title="Bill By Employee" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">
            function doPrint() {
                var prtContent = document.getElementById('<%= ReportViewer1.ClientID %>');
        prtContent.border = 0; //set no border here
        var WinPrint = window.open('', '', 'left=150,top=100,width=1000,height=1000,toolbar=0,scrollbars=1,status=0,resizable=1');
        WinPrint.document.write(prtContent.outerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
        WinPrint.close();
    }
   </script> 
    <table style="width: 100%;" align="left"  >
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:12pt; font-weight:bold; color:#000080; font-family:Courier New;" dir="rtl" 
            >
        &nbsp; Billing Data By Employee </span>
    </td></tr>
 </table>
        <br />
        <br />
          <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server"> </asp:ScriptManager>
        <br />
  
 <table  align="left" width="100%"  dir="ltr" 
        style="margin-top: 30px; margin-right: 30px; margin-left: 30px">
        <tr>
            <td align="left" dir="ltr">
              
                <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="Employee" Width="120px"></asp:Label>
                    <asp:DropDownList ID="dd_emp_code" runat="server" Font-Bold="True" 
                        ForeColor="#3366FF" Width="270px" Font-Size="13pt">
                    </asp:DropDownList>
     
            </td>
        </tr>
        <tr>
            <td align="left" dir="ltr">
              
                &nbsp;<br />
              
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="From Month" Width="120px"></asp:Label>
                <asp:TextBox ID="txt_Fmonth" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Height="27px" Width="81px"></asp:TextBox>
              
                <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="To Month" Width="100px"></asp:Label>
                <asp:TextBox ID="txt_Tmonth" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Height="27px" Width="81px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" 
                        ControlToValidate="txt_Fmonth" ErrorMessage="Please enter month number" 
                        MaximumValue="12" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" 
                        ControlToValidate="txt_Tmonth" ErrorMessage="Please enter month number" 
                        MaximumValue="12" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                <br />
         
                <br />
     
            </td>
        </tr>
        <tr>
            <td align="left" dir="ltr">
              
                <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="From Year" Width="120px"></asp:Label>
                <asp:TextBox ID="txt_Fyear" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Height="27px" Width="81px"></asp:TextBox>
              
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="To Year" Width="100px"></asp:Label>
                <asp:TextBox ID="txt_Tyear" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Height="27px" Width="81px"></asp:TextBox>
     
                    <asp:RangeValidator ID="RangeValidator2" runat="server" 
                        ControlToValidate="txt_Fyear" ErrorMessage="Please enter the year" 
                        MaximumValue="2100" MinimumValue="2000" Type="Integer"></asp:RangeValidator>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" 
                        ControlToValidate="txt_Tyear" ErrorMessage="Please enter the year" 
                        MaximumValue="2100" MinimumValue="2000" Type="Integer"></asp:RangeValidator>
     
            </td>
        </tr>
        <tr>
            <td align="left" dir="ltr">
              
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left">
         
        <asp:Button ID="btn_show" runat="server" Font-Bold="True" Font-Size="12pt" 
            onclick="btn_show_Click" Text="Display Report" Width="250px" />
     
            </td>
        </tr>
        <tr>
            <td align="left">
         
    <asp:Button ID="PrintButton" runat="server" Text="Print" OnClientClick="doPrint();" 
            ToolTip="Print Report" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" 
                      Width="250px" />
     
            </td>
        </tr>
        <tr>
            <td align="left">
         
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="500px" 
                    Width="80%">
                </rsweb:ReportViewer>
     
            </td>
        </tr>
        <tr align="right" dir="rtl">
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
 
        <br />
        <br />
</asp:Content>

