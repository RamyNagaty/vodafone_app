﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="bill_by_number.aspx.cs" Inherits="Reports_bill_by_number" Title="Billing By Bill Number Report" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">
            function doPrint() {
                var prtContent = document.getElementById('<%= ReportViewer1.ClientID %>');
        prtContent.border = 0; //set no border here
        var WinPrint = window.open('', '', 'left=150,top=100,width=1000,height=1000,toolbar=0,scrollbars=1,status=0,resizable=1');
        WinPrint.document.write(prtContent.outerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
        WinPrint.close();
    }
   </script> 
    <table style="width: 100%;" align="left"  >
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:12pt; font-weight:bold; color:#000080; font-family:Courier New;" 
            >
        &nbsp; Billing Data By Bill Number </span>
    </td></tr>
 </table>
        <br />
        <br />
          <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server"> </asp:ScriptManager>
        <br />
  
 <table  align="left" width="100%"  dir="ltr" 
        style="margin-top: 30px; margin-right: 30px; margin-left: 30px">
        <tr>
            <td align="left" dir="ltr">
              
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Text="Bill Number "></asp:Label>
                <br />
                <asp:TextBox ID="txt_bill_no" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Height="27px"></asp:TextBox>
                <br />
         
                <br />
     
            </td>
        </tr>
        <tr>
            <td align="left">
         
        <asp:Button ID="btn_show" runat="server" Font-Bold="True" Font-Size="12pt" 
            onclick="btn_show_Click" Text="Display Report" Width="150px" />
     
            </td>
        </tr>
        <tr>
            <td align="left">
         
    <asp:Button ID="PrintButton" runat="server" Text="Print" OnClientClick="doPrint();" 
            ToolTip="Print Report" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" 
                      Width="150px" />
     
            </td>
        </tr>
        <tr>
            <td align="left">
         
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="500px" 
                    Width="80%">
                </rsweb:ReportViewer>
     
            </td>
        </tr>
        <tr align="right" dir="rtl">
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
 
        <br />
        <br />
</asp:Content>

