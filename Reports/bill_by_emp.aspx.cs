﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using System.Xml.Linq;
using System.Data.SqlClient;
using Vodafone_bill_WS;

public partial class Reports_bill_by_emp : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            Ds = ser.Bind_EMP_DDL();
            dd_emp_code.DataSource = Ds.Tables[0];

            dd_emp_code.DataTextField = "EMP_NAME";
            dd_emp_code.DataValueField = "EMP_NO";
            dd_emp_code.DataBind();
            ListItem lst = new ListItem("***** Select Employee *****", "0");
            dd_emp_code.Items.Insert(0, lst);
            //**********************************
        }
        check_security();
    }
    public void check_security()
    {
        try
        {
            string page_name = GetCurrentPageName();
            int Display = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DISPLAY");
            int Save = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "SAVE");
            int Update = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "UPDATE");
            int Delete = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DELETE");
            int All = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "ALL");

            if (All > 0)
            {
                //Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
            }
            else
            {
                if (Display > 0)
                {

                    //*******************************
                    if (Save > 0)
                    { //btn_save.Visible = true;
                    }
                    else
                    { //btn_save.Visible = false; 
                    }
                    //******************************
                    if (Update > 0)
                    { //btn_update.Visible = true; 
                    }
                    else
                    { //btn_update.Visible = false; 
                    }
                    //*******************************
                    if (Delete > 0)
                    { //btn_delete.Visible = true;
                    }
                    else
                    { //btn_delete.Visible = false; 
                    }
                    //*********************************

                }
                else
                {
                    Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
                }

            }
        }
        catch
        {

        }
    }
    public string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }
    protected void btn_show_Click(object sender, EventArgs e)
    {
        if (dd_emp_code.SelectedIndex == 0)
        {
            Alert.Show("Sorry You Must Select Employee Name .");
            return;
        }
        else
        {
            DataTable ddt = new DataTable();
            ddt.Clear();
            ddt = ser.Get_Bill_BY_EMP(dd_emp_code.SelectedValue.ToString(), txt_Fmonth.Text, txt_Tmonth.Text, txt_Fyear.Text, txt_Tyear.Text).Tables[0];
            Generatereport(ddt);
        }
    }

    private void Generatereport(DataTable dt)
    {
        ReportViewer1.SizeToReportContent = true;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/bill_by_emp_rep.rdlc");
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportDataSource _rsource = new ReportDataSource("ROI_DataSet_BILL_DATA_EMP", dt);
        ReportViewer1.LocalReport.DataSources.Add(_rsource);

        ReportParameter rp1 = new ReportParameter("emp_name", dd_emp_code.SelectedItem.ToString() , false);
        ReportParameter rp2 = new ReportParameter("fmonth", txt_Fmonth.Text, false);
        ReportParameter rp3 = new ReportParameter("tmonth", txt_Tmonth.Text, false);
        ReportParameter rp4 = new ReportParameter("fyear", txt_Fyear.Text, false);
        ReportParameter rp5 = new ReportParameter("tyear", txt_Tyear.Text , false);

        this.ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp1 , rp2 , rp3 , rp4 , rp5 });

        ReportViewer1.LocalReport.Refresh();

    }
}
