﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using System.Xml.Linq;
using System.Data.SqlClient;
using Vodafone_bill_WS;

public partial class Reports_bill_by_date : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
        }
        check_security();
    }
    public void check_security()
    {
        try
        {
            string page_name = GetCurrentPageName();
            int Display = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DISPLAY");
            int Save = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "SAVE");
            int Update = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "UPDATE");
            int Delete = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DELETE");
            int All = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "ALL");

            if (All > 0)
            {
                //Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
            }
            else
            {
                if (Display > 0)
                {

                    //*******************************
                    if (Save > 0)
                    { //btn_save.Visible = true; 
                    }
                    else
                    { //btn_save.Visible = false; 
                    }
                    //******************************
                    if (Update > 0)
                    { //btn_update.Visible = true;
                    }
                    else
                    { //btn_update.Visible = false; 
                    }
                    //*******************************
                    if (Delete > 0)
                    { //btn_delete.Visible = true; 
                    }
                    else
                    { //btn_delete.Visible = false; 
                    }
                    //*********************************

                }
                else
                {
                    Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
                }

            }
        }
        catch
        {

        }
    }
    public string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }
    protected void btn_show_Click(object sender, EventArgs e)
    {
        if (txt_Month.Text == "" || txt_year.Text == "")
        {
            Alert.Show("Sorry You Must write Month - Year  .");
            return;
        }
        else
        {
            DataTable ddt = new DataTable();
            ddt.Clear();
            ddt = ser.Get_Bill_BY_DATE(txt_Month.Text , txt_year.Text ).Tables[0];
            Generatereport(ddt);
        }
    }
    private void Generatereport(DataTable dt)
    {
        //ReportViewer1.ProcessingMode = ProcessingMode.Local;
        //LocalReport localReport = ReportViewer1.LocalReport;
        //ReportViewer1.Height = Unit.Parse("100%");
        //ReportViewer1.Width = Unit.Parse("100%");
        //ReportViewer1.CssClass = "table";
        ReportViewer1.SizeToReportContent = true;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/bill_by_no_rep.rdlc");
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportDataSource _rsource = new ReportDataSource("ROI_DataSet_BILL_DATA", dt);
        ReportViewer1.LocalReport.DataSources.Add(_rsource);

        ReportParameter rp = new ReportParameter("bill_no", txt_Month.Text, false);
        // ReportParameter rp2 = new ReportParameter("Dist_Code", Session["Dist_ID"].ToString(), false);
        this.ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp });

        ReportViewer1.LocalReport.Refresh();

    }
}
