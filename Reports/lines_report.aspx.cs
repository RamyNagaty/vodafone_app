﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using System.Xml.Linq;
using System.Data.SqlClient;
using Vodafone_bill_WS;

public partial class Reports_lines_report : System.Web.UI.Page
{
    Vodafone_bill_WS.Service ser = new Service();
    DataSet Ds = new DataSet();
    DataSet Ds2 = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            Ds = ser.Bind_EMP_DDL();
            dd_emp_code.DataSource = Ds.Tables[0];

            dd_emp_code.DataTextField = "EMP_NAME";
            dd_emp_code.DataValueField = "EMP_NO";
            dd_emp_code.DataBind();
            ListItem lst = new ListItem("***** All Employees *****", "0");
            dd_emp_code.Items.Insert(0, lst);
            //**********************************
            Ds2 = ser.Bind_Dep_DDL();
            dd_dept.DataSource = Ds2.Tables[0];
            dd_dept.DataTextField = "SECTION_NAME";
            dd_dept.DataValueField = "SECTION_NO";
            dd_dept.DataBind();
            ListItem lstt = new ListItem("***** All Departments *****", "0");
            dd_dept.Items.Insert(0, lstt);
            //**********************************
        }

        check_security();
    }
    public void check_security()
    {
        try
        {
            string page_name = GetCurrentPageName();
            int Display = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DISPLAY");
            int Save = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "SAVE");
            int Update = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "UPDATE");
            int Delete = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "DELETE");
            int All = ser.Check_Page_Security(page_name, Session["Logged_User"].ToString(), "ALL");

            if (All > 0)
            {
                //Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
            }
            else
            {
                if (Display > 0)
                {

                    //*******************************
                    if (Save > 0)
                    { //btn_save.Visible = true; 
                    }
                    else
                    { //btn_save.Visible = false; 
                    }
                    //******************************
                    if (Update > 0)
                    { //btn_update.Visible = true; 
                    }
                    else
                    { //btn_update.Visible = false; 
                    }
                    //*******************************
                    if (Delete > 0)
                    { //btn_delete.Visible = true; 
                    }
                    else
                    { //btn_delete.Visible = false; 
                    }
                    //*********************************

                }
                else
                {
                    Response.Redirect("~/Setting/Unauthorized_Pg.aspx");
                }

            }
        }
        catch
        {

        }
    }
    public string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }
    private string get_finished()
    {
        string xx = "0";
        if (dd_finished.Checked == true)
        {
            xx = "1";
        }
        else
        {
            xx = "2";
        }

        return xx;
    }
    protected void btn_show_Click(object sender, EventArgs e)
    {
      
        DataTable ddt = new DataTable();
        ddt.Clear();
        string finished = get_finished();
        ddt = ser.Get_Lines(dd_emp_code.SelectedValue.ToString(), dd_dept.SelectedValue.ToString(), Get_Status(), dd_line_type.SelectedValue.ToString(), finished ).Tables[0];
        Generatereport(ddt);
       
    }

    private string Get_Status()
    {
        string XX = "1";
        try
        {
            if (dd_status.Checked == true)
                {return "0";}
            else
                { return "1"; } 
        }
        catch
        {
            XX = "1";
        }
        return XX;
    }

    private void Generatereport(DataTable dt)
    {
        
        ReportViewer1.SizeToReportContent = true;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Lines_data_rep.rdlc");
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportDataSource _rsource = new ReportDataSource("ROI_DataSet_LINES_DATA", dt);
        ReportViewer1.LocalReport.DataSources.Add(_rsource);

       // ReportParameter rp = new ReportParameter("bill_no", txt_Month.Text, false);
        // ReportParameter rp2 = new ReportParameter("Dist_Code", Session["Dist_ID"].ToString(), false);
       // this.ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp });

        ReportViewer1.LocalReport.Refresh();

    }
    protected void dd_line_type_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void dd_dept_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void dd_status_CheckedChanged(object sender, EventArgs e)
    {

    }
}
